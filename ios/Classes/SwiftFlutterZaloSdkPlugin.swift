import Flutter
import UIKit
import ZaloSDK

public class SwiftFlutterZaloSdkPlugin: NSObject, FlutterPlugin {
    
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "flutter_zalo_sdk", binaryMessenger: registrar.messenger())
    let instance = SwiftFlutterZaloSdkPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
    
    /// Zalo sdk
   if let zaloAppID = Bundle.main.infoDictionary?["ZaloAppID"] as? String {
        NSLog("aloAppID: " + zaloAppID)
        ZaloSDK.sharedInstance()?.initialize(withAppId: zaloAppID)
    }
    else
    {
        if #available(iOS 10.0, *) {
            NSLog("You haven't added ZaloAppID to Info.plist")
        } else {
            NSLog("You haven't added ZaloAppID to Info.plist")
        }
    }
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    switch call.method {
    case "postFeed":
        let arguments = call.arguments as! Dictionary<String, Any>
                let message = arguments["message"] as? String
                let link = arguments["link"] as? String
                let appName = arguments["appName"] as? String
                let others: Dictionary<String, String> = [:]
        let feed = ZOFeed(
                link: link,
                appName: appName,
                message: message,
                others: others
        )
        let rootViewController  = UIApplication.shared.keyWindow?.rootViewController
        NSLog("argument " + link!)
        ZaloSDK.sharedInstance().share(feed, in: rootViewController) { (response) in
            if response != nil && response!.isSucess{
                // Thông tin đã được gửi đến Zalo thành công
                result(true)
                
            }
        }
        break;
    case "isZaloInstalled":
        let appName = "zalo"
        let appScheme = "\(appName)://app"
        let appUrl = URL(string: appScheme)

        NSLog("isZaloInstalledObs is: " + appScheme)
        
        if UIApplication.shared.canOpenURL(appUrl! as URL) {
            NSLog("isZaloInstalledObs is avalable")
            result(true)
        } else {
            NSLog("isZaloInstalledObs is unavalable")
            result(false)
        }
        break;
    default:
        break;
    }
  }
}
