import 'dart:async';

import 'package:flutter/services.dart';

class FlutterZaloSdk {
  static const MethodChannel _channel = MethodChannel('flutter_zalo_sdk');

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  /// Calls the platform-specific function to send the app to the background
  static Future<bool> postFeed(FeedData feedData) async =>
      await _channel.invokeMethod('postFeed', feedData.toJson());

  /// Calls the platform-specific function to send the app to the background
  static Future<bool> isZaloInstalled() async =>
      await _channel.invokeMethod('isZaloInstalled');
}

class FeedData {
  String message;
  String link;
  String linkTitle;
  String linkSource;
  String linkThumb;

  FeedData(
      this.message, this.link, this.linkTitle, this.linkSource, this.linkThumb);

  Map<String, dynamic> toJson() => <String, dynamic>{
        'message': message,
        'link': link,
        'linkTitle': linkTitle,
        'linkSource': linkSource,
        'linkThumb': linkThumb,
      };
}
