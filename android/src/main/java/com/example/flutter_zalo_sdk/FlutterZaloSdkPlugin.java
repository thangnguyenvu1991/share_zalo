package com.example.flutter_zalo_sdk;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import com.zing.zalo.zalosdk.oauth.FeedData;
import com.zing.zalo.zalosdk.oauth.OpenAPIService;
import com.zing.zalo.zalosdk.core.helper.Utils;
import com.zing.zalo.zalosdk.oauth.ZaloPluginCallback;

import java.util.logging.Logger;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

/** FlutterZaloSdkPlugin */
public class FlutterZaloSdkPlugin implements FlutterPlugin, MethodCallHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private MethodChannel channel;
  private OpenAPIService zaloApiService = new OpenAPIService();
  private Context context;

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "flutter_zalo_sdk");
    channel.setMethodCallHandler(this);
    this.context = flutterPluginBinding.getApplicationContext();
  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    switch (call.method){
      case "getPlatformVersion":
        result.success("Android " + android.os.Build.VERSION.RELEASE);
        break;
      case "postFeed":
        postFeed(call,result);
        break;
      case "isZaloInstalled":
        result.success(_isZaloInstalled(context));
        break;
      default:
        result.notImplemented();
        break;
    }
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    channel.setMethodCallHandler(null);
  }

  private void postFeed(MethodCall call, Result result) {
    final String message = call.argument("message");
    final String link = call.argument("link");
    final String linkTitle = call.argument("linkTitle");
    final String linkSource = call.argument("linkSource");
    final String linkThumb = call.argument("linkThumb");


    FeedData feed = new FeedData();
    feed.setMsg(message);
    feed.setLink(link);
    feed.setLinkTitle(linkTitle);
    feed.setLinkSource(linkSource);
    feed.setLinkThumb(new String[] {linkThumb});
    zaloApiService.shareFeed(context, feed, (var1, var2, var3, var4) -> {
      Log.i("FlutterZaloPlugin","var1 " + var1);
      Log.i("FlutterZaloPlugin","var2 " + var2);
      Log.i("FlutterZaloPlugin","var3 " + var3);
      Log.i("FlutterZaloPlugin","var4 " + var4);
      result.success(true);
    });
  }

  private boolean _isZaloInstalled(Context context) {
    Intent var10000 = new Intent("android.intent.action.SEND");
    var10000.setComponent(new ComponentName("com.zing.zalo", "com.zing.zalo.ui.TempShareViaActivity"));
    return var10000.resolveActivityInfo(context.getPackageManager(), 0) != null;
  }
}
